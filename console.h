#ifndef CONSOLE
#define CONSOLE
#include <ncurses.h>
#include <string.h>
#include <stdlib.h>
#include "parse_command.h"


#define MAXBUFFERSIZE 500

int win_col, win_row;

typedef struct line  //struktura trzymajaca linie o znormalizowanej dlugosci (maks szerokosc ekranu)
{
  char  text[255]; //text lini, max 255, zakladam ze wiecej nie bedzie
  struct line * next;
  struct line * prev;
} line;

typedef struct console_screen
{
  int lineWidth; //szereokosc lini w terminalu
  int maxLines; // ilosc wierszy w terminalu
  int lines; //aktualna ilosc trzymanych w pamieci linii teksty do wyswietlenia
  line * firstLine; //wskaznik na liste z liniami
  line * lastLine; //wskaznik do ostatniego elementu listy
} console_screen;

typedef struct buffer
{
  char sign; //znak zachety

  int length; //dlugosc stringa (ilosc znakow)
  int size; //rozmiar tablicy
  char str[MAXBUFFERSIZE];
  int lines; //ile lini aktualnie zajmuje buffer
  int maxLines; //ile lini maksymalnie zajmowal buffer
} buffer;

//done
buffer * newBuffer(const char *); //tworzy nowy pusty egzemplarz buffer
int addChar(buffer *, const char); //funkcja dodaje na koncu buffer jeden znak char
int delChar(buffer* ); //funkcja usuwa ze buffer ostatni znak chyba ze zostal jeden
int popFirstLine(console_screen *); // funkcja zrzuca pierwszy element z listy
int popLastLine(console_screen *); //funkcja zrzuca ostatni element z listy
console_screen * newConsoleScreen(int maxLines, int lineWidth);
int showScreen(WINDOW *,buffer *, console_screen *); //funkcja ktora rysuje buffer (nie dodaje go jeszcze do lines)
int pullString(char *, console_screen *); //funkcja zrzuca buffer do listy lines
int clearBuffer(buffer *); //czysci buffer
int clearLines(console_screen * cs); //czysci liste z liniami
void Console(); //glowna funkcja konsoli, sterowanie i wyswietlanie
char * buffToString(buffer *);

#endif
