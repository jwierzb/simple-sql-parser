#include "calculator.h"

bracketStack * STACK;
#define PRECISION 60

char * calculate(char * exp)
{
  char  tmp[500],  tmp_2[1000], c;
  //tmp=malloc(sizeof(char)*(strlen(exp)*2+PRECISION+1)); //rezerwuje miejsce dla odpowiednio wiekszej liczby
  //tmp_2=malloc(sizeof(char)*(strlen(exp)));

  float k,l;
  if(exp==NULL)
  {
    return NULL;
  }
  if(isFloat(exp))
  {
    //strcpy(tmp_2, exp);
    //exp=malloc(sizeof(tmp_2));
    //strcpy(exp, tmp_2);
    return exp;
  }
  if(exp[0]=='*' || exp[0]=='/' ) return "ERROR";
  if(!strcmp(exp, "ERROR")) return "ERROR";
  if(exp[0]=='(')
  {
    push('(');
    return calculate(exp+1);
  }
  if(exp[0]==')')
  {
    if(pop()!='(')
    {
      return "ERROR";
    }
    else
    {
      if(isNull())
      return calculate(exp+1);
      return  exp+1;
    }
  }

  /////////////////////////////////////////////////////////////
  if(sscanf(exp, "%f)%s", &l, tmp)==2)
  {
    if(!strcmp(tmp, "ERROR"))
    {
      eraseStack();
      return "ERROR";
    }
    sprintf(tmp_2, ")%f%s", l,tmp);
    strcpy(exp, tmp_2);
    return calculate(exp);
  }


  if(sscanf(exp, "%f%c", &l, &c)==2 && c==')')
  {
    sprintf(tmp_2, "%f", l);
    exp=malloc(sizeof(tmp_2));
    strcpy(exp, tmp_2);
    return exp;
  }
  ////////////////////////////////////////////////////////////

  if(sscanf(exp, "%f*%f%s", &k, &l, tmp)==2) //mnozenie
  {
    if(sscanf(exp, "%f*%f", &k, &l)==2)
    {
      sprintf(tmp_2, "%f", k*l);
      exp=malloc(sizeof(tmp_2));
      strcpy(exp, tmp_2);
      return exp;
    }
  }
  if(sscanf(exp, "%f/%f%s", &k, &l, tmp)==2) //dzielenie
  {
    if(sscanf(exp, "%f/%f", &k, &l)==2)
    {
      sprintf(tmp_2,  "%f", k/l);
      exp=malloc(sizeof(tmp_2));
      strcpy(exp, tmp_2);
      return exp;
    }
  }
  if(sscanf(exp, "%f*%f%s", &k, &l, tmp)==3) //mnozenie
  {
    if(!strcmp(tmp, "ERROR"))
    {
      eraseStack();
      return "ERROR";
    }
    sprintf(tmp_2,  "%f%s", k*l,tmp);
    return calculate(tmp_2);
  }
  if(sscanf(exp, "%f/%f%s", &k, &l, tmp)==3) //dzielenie
  {
    if(!strcmp(tmp, "ERROR"))
    {
      eraseStack();
      return "ERROR";
    }
    sprintf(tmp_2, "%f%s", k/l,tmp);
    return calculate(tmp_2);
  }
  if(sscanf(exp, "%f*%s", &k, tmp)==2) //mnozenie
  {
    if(!strcmp(tmp, "ERROR"))
    {
      eraseStack();
      return "ERROR";
    }
    sprintf(tmp_2, "%f*%s", k, calculate(tmp));
    return calculate(tmp_2);
  }
  if(sscanf(exp, "%f/%s", &k,  tmp)==2 ) //dzielenie
  {
    if(!strcmp(tmp, "ERROR"))
    {
      eraseStack();
      return "ERROR";
    }
    sprintf(tmp_2,  "%f/%s", k,calculate(tmp));
    return calculate(tmp_2);
  }

if(sscanf(exp, "%f+%f%s", &k, &l, tmp)==3 ) //dodawanie
{

  if(tmp[0]==')' || tmp[0]=='+' ||tmp[0]=='-')
  {
    if(!strcmp(tmp, "ERROR"))
    {
      eraseStack();
      return "ERROR";
    }
    sprintf(tmp_2, "%f%s",k+l,tmp);
    return calculate(tmp_2);
  }
}
if(sscanf(exp, "%f-%f%s", &k,&l, tmp)==3) //odejmowanie
{

  if(tmp[0]==')'|| tmp[0]=='+' ||tmp[0]=='-')
  {
    if(!strcmp(tmp, "ERROR"))
    {
      eraseStack();
      return "ERROR";
    }
    sprintf(tmp_2, "%f%s",k-l,tmp);
    return calculate(tmp_2);
  }
}
/////////////////////////////////////////////////////////////
if(sscanf(exp, "%f+%f%s", &k, &l, tmp)==3) //dodawanie
{
  sscanf(exp, "%f+%s", &k,tmp);
  //char * float_tmp=calculate(tmp);
  sprintf(tmp_2,  "%f+%s",k, calculate(tmp));
  return calculate(tmp_2);
}
if(sscanf(exp, "%f-%f%s", &k,&l, tmp)==3) //odejmowanie
{
  sscanf(exp, "%f-%s", &k, tmp);
  //char * float_tmp=calculate(tmp);
  sprintf(tmp_2,  "%f-%s",k, calculate(tmp));
  return calculate(tmp_2);
}
/////////////////////////////////////////////////////////////
if(sscanf(exp, "%f+%f%s", &k, &l, tmp)==2) //dodawanie
{
  if(sscanf(exp, "%f+%f", &k, &l)==2)
  {
    if(!strcmp(tmp, "ERROR"))
    {
      eraseStack();
      return "ERROR";
    }
    sprintf(tmp_2,  "%f",k+l);
    exp=malloc(sizeof(tmp_2));
    strcpy(exp, tmp_2);
    return exp;
  }
}
if(sscanf(exp, "%f-%f%s", &k,&l, tmp)==2) //odejmowanie
{
  if(sscanf(exp, "%f-%f", &k, &l)==2)
  {
    if(!strcmp(tmp, "ERROR"))
    {
      eraseStack();
      return "ERROR";
    }
    sprintf(tmp_2,  "%f",k-l);
    exp=malloc(sizeof(tmp_2));
    strcpy(exp, tmp_2);
    return exp;
  }
}
/////////////////////////////////////////////////////////////
if(sscanf(exp, "%f+%s", &k, tmp)==2 ) //dodawanie
{
  if(!strcmp(tmp, "ERROR"))
  {
    eraseStack();
    return "ERROR";
  }
  char * float_tmp=calculate(tmp);
  sprintf(tmp_2, "%f+%s", k,float_tmp);
  return calculate(tmp_2);
}
if(sscanf(exp, "%f-%s", &k, tmp)==2) //odejmowanie
{
  if(!strcmp(tmp, "ERROR"))
  {
    eraseStack();
    return "ERROR";
  }
  char * float_tmp=calculate(tmp);
  sprintf(tmp_2, "%f-%s", k,float_tmp);
  return calculate(tmp_2);
}


return "ERROR";
}

int isFloat(char * c)
{
  float f;
  char * str=malloc(sizeof(char)*strlen(c));
  int i;
  if(sscanf(c, "%f%s", &f, str)==1)
  {
    if(sscanf(c, "%f", &f)==1)
    {
      free(str);
      return 1;
    }
  }
  if(str) free(str);
  return 0;
}

int push(char c)
{
  if(c=='\0') return 0;

  bracketStack *tmp;
  if(!(tmp=malloc(sizeof(bracketStack)))) return 0;
  tmp->val=c;
  if(STACK==NULL)
  {
    STACK=tmp;
    tmp->prev=NULL;
    return 1;
  }

  tmp->prev=STACK;
  STACK=tmp;
  return 1;
}

char pop()
{
  if(STACK==NULL) return '\0';
  char c=STACK->val;
  bracketStack *tmp=STACK;
  STACK=STACK->prev;
  free(tmp);
  return c;
}

int eraseTab(char * c)
{
  int i=0;
  if(c==NULL) return 0;
  while(c[i])
  {
    c[i]='\0';
    i++;
  }
  return 1;

}
int isNull()
{
  if(STACK==NULL) return 1;
  return 0;
}
int eraseStack()
{
  if(!pop()) return 0;
  while(pop());
  return 1;
}
