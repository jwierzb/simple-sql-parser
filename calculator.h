#ifndef CALCULATOR
#define CALCULATOR



#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct bracketStack //stos do liczenia nawiasow
{
  char val;
  struct bracketStack * prev;
} bracketStack;

int push(char c);
char pop();
int isNull();
int eraseStack();

int eraseTab(char *);




char * calculate(char *);
int isFloat(char *);


#endif
