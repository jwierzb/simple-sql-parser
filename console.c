#include "console.h"

#define ALT_BACKSPACE 127

void Console()
{
  int key=0;
  char * communicate;
  char * command;

  command=NULL;
  newterm(NULL, stderr, stdin);
  getmaxyx(stdscr, win_row, win_col);
  WINDOW * console=newpad(win_row,win_col);

  refresh();
  cbreak();
  noecho();
  clear();

  curs_set(2);
  buffer * buff=newBuffer(">");
  console_screen *cs=newConsoleScreen(win_row,win_col);


  keypad(console,TRUE);


//  pullString(buff->str,cs);
  showScreen(console,buff,cs);
  prefresh(console,0,0,0,0,win_row,win_col);

  while(1)
  {
    key=0;
    while(key!=10){
    if(command!=NULL) free(command);
    key=wgetch(console);
    if((key>=33 && key<=126) || key==' ') addChar(buff, key);
    if(key==ALT_BACKSPACE) delChar(buff);
    if(key==10)
    {
      if(pullString(buff->str,cs))
      {
        if(cs->lines==cs->maxLines) popFirstLine(cs);
        if(!strcmp(buff->str,">exit")|| !strcmp(buff->str,">q"))
        {
          delwin(console);
          endwin();
          clearLines(cs);
          free(cs);
          free(buff);
          return;
        } //wyjscie z programu
        ///////tutaj prasowanie polecenia
        if(command=buffToString(buff))
        {
          if(communicate=parseCommand(command))
          {
            pullString(communicate,cs);
            communicate=NULL;
          }
        }
        command=NULL;
        clearBuffer(buff);
        //free(communicate);
      }
    }

    werase(console);
    showScreen(console,buff,cs);
    prefresh(console,0,0,0,0,win_row,win_col);
    }
  }
  delwin(console);
  endwin();
  return;
}
int clearBuffer(buffer * buff)
{
  int i=1;
  while(i<MAXBUFFERSIZE)
  {
    buff->str[i]='\0';
    i++;
  }
  buff->length=strlen(buff->str);
  buff->lines=1;
  buff->maxLines=1;
  return 1;
}
console_screen * newConsoleScreen(int maxLines, int lineWidth)
{
  console_screen * tmp=malloc(sizeof(console_screen));
  tmp->maxLines=maxLines;
  tmp->lineWidth=lineWidth;
  tmp->lines=0;
  tmp->firstLine=NULL;
  tmp->lastLine=NULL;
  return tmp;
}


int showScreen(WINDOW *console, buffer * buff, console_screen * cs)
{
  int i=buff->lines;
  line * tmp=cs->firstLine;
  i=buff->lines+cs->lines-cs->maxLines;
  if(i<0) i=0;
  while(i>0)
  {
    if(tmp!=NULL) tmp=tmp->next;
    i--;
  }
  while(tmp!=NULL)
  {
    wprintw(console, "%s\n", tmp->text);
    tmp=tmp->next;
  }
  wprintw(console, "%s", buff->str);

  return 1;

}
int pullString(char * str, console_screen * cs)
{
  int i, j;
  i=0;
  j=0;
  line * tmp;
  if(strlen(str)<1) return 1;
  if(cs->firstLine==NULL)
  {
    cs->firstLine=malloc(sizeof(line));
    cs->firstLine->next=NULL;
    tmp=cs->firstLine;
    cs->lines=cs->lines+1;

  }
  else{
    tmp=cs->firstLine;
    while(tmp->next!=NULL)tmp=tmp->next;
    tmp->next=malloc(sizeof(line));
    tmp=tmp->next;
    cs->lines=cs->lines+1;

    tmp->next=NULL;
  }
    //brak inicjalizacji tablicy tmp->text
    while(str[i]!='\0')
    {
      if(str[i]=='\n' || j==cs->lineWidth-1 )
      {
        tmp->next=malloc(sizeof(line));
        tmp=tmp->next;
        tmp->next=NULL;
        cs->lines=cs->lines+1;
        j=0;
        i++;
        continue;
      }
      tmp->text[j]=str[i];
      j++;
      i++;
    }
    cs->lastLine=tmp;
    return 1;
  }




buffer * newBuffer(const char * c)
{
  buffer * tmp=malloc(sizeof(buffer));
  strcpy(tmp->str, c);
  tmp->size=MAXBUFFERSIZE;
  tmp->length=strlen(c);
  tmp->lines=1;
  tmp->maxLines=1;
  return tmp;
}

int addChar(buffer * buff, const char c)
{
    if(buff->length==buff->size) return 0;
    else
    {
      buff->str[buff->length]=c;
      buff->length+=1;
      buff->lines=buff->length/win_col + 1;
      if(buff->lines>buff->maxLines) buff->maxLines=buff->lines;
      return 1;
    }
}

int delChar(buffer * buff)
{
    if(buff->length<=1)
    {
      buff->str[1]='\0';
      return 1;
    }
    else
    {
      buff->str[buff->length-1]='\0';
      buff->length-=1;
      buff->lines=buff->length/win_col+1;
      return 1;
    }
    return 0;
}
int popFirstLine(console_screen * cs)
{
  if(cs->firstLine==NULL) return 1;
  if(cs->firstLine==cs->lastLine)
  {
    free(cs->firstLine);
    cs->firstLine=NULL;
    cs->lastLine=NULL;
    cs->lines=0;
    return 1;
  }
  line *tmp=cs->firstLine;
  cs->firstLine=tmp->next;
  cs->lines-=1;
  free(tmp);
  return 1;
}
int clearLines(console_screen * cs)
{
  while(cs->firstLine!=NULL)
  {
    if(!popLastLine(cs)) return 0;
  }
  return 1;
}
int popLastLine(console_screen * cs)
{
  if(cs->lastLine==NULL) return 1;
  if(cs->firstLine==cs->lastLine)
  {
    free(cs->firstLine);
    cs->firstLine=NULL;
    cs->lastLine=NULL;
    cs->lines=0;

    return 1;
  }
  line *tmp=cs->firstLine;
  while(tmp->next!=cs->lastLine) tmp=tmp->next;
  cs->lastLine=tmp;
  free(tmp->next);
  cs->lines-=1;
  return 1;
}


char * buffToString(buffer * buff)//funkcja usuwa znak zachety z buffera
{
  if(!strcmp(buff->str,">")) return NULL;
  char * tmp=malloc(sizeof(char)*(buff->length));
  int i=1;
  while(buff->str[i])
  {
    tmp[i-1]=buff->str[i];
    i++;
  }
  return tmp;
}
