#ifndef TABLE
#define TABLE
#include <stdio.h>
#include <stdlib.h>

//types

typedef struct column
{
  char type[100]; //int, string[250], enum{wartocsi kolejno}, join
  char name[25];
  char default_value[25]; // domyslna wartosc, domyslnie NULL
  int primary_key; //tylko jedna kolumna w tabeli moze miec to pole ==1
  int auto_increment; //0 jesli wylaczone, jesli >= 1 increment od tej wartosci
} column;

typedef struct table{
  char  name[250];
  column * columns;
  char ** rows;
} table;

table * TABLES;
int TABLES_COUNT;

table * newEmptyTable();
int * addRow(char ** string_tab);
int * modifyRow(char * command);
int * addColumn(char * name, char * type);
int * delColumn(char * name);
int * modifyColumn(char * name, char * value);
int * createTable(char ** command);

#endif
